import { Inject, Injectable } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { lastValueFrom } from 'rxjs';
import { BILLING_SERVICE } from '../constants/service';
import { CreateOrderRequest } from './dto/create-order-request';
import { OrderRepository } from './orders.respository';

@Injectable()
export class OrdersService {
  constructor(
    private readonly ordersRepository: OrderRepository,
    @Inject(BILLING_SERVICE) private billingClient: ClientProxy,
  ) {}

  async createOrder(request: CreateOrderRequest, authentication: string) {
    // return this.ordersRepository.create(request);
    console.log(request);

    const session = await this.ordersRepository.startTransaction();
    try {
      const order = await this.ordersRepository.create(request, { session });
      await lastValueFrom(
        this.billingClient.emit('order_created', {
          request,
          Authentication: authentication,
        }),
      );
      await session.commitTransaction();
      // console.log('===>> order', order);

      return order;
    } catch (error) {
      await session.abortTransaction();
      throw error;
    }
  }

  async listAllOrder() {
    const result = await this.ordersRepository.find({});
    const count = (await this.ordersRepository.find({})).length;
    return { result, count };
  }
}
